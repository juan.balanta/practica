import { Producto, calculaISV } from './06-desestructuracion-arreglos';


/*
    ===== Código de TypeScript =====


    Si necesitas importar una interfaz, debes darle export interface en el archivo original
    Luego puedes usar Ctrl + . para elegir la opcion Add import from ....
*/


const carritoCompras: Producto[] = [
    {
        desc: 'Telefono 1',
        precio: 100
    },
    {
        desc: 'Telefono 2',
        precio: 150
    },
];

const [total, isv] = calculaISV(carritoCompras);

console.log('Total: ', total);
console.log('ISV: ', isv)