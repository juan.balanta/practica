/*
    ===== Código de TypeScript =====
*/

import { isConstructorDeclaration } from "typescript";

function sumar(a: number ,b: number): number{
    return (a+b);
}

const sumarFlecha = (a:number, b:number):number => {
    return a + b
}


function multiplicar( numero:number , otroNumero?:number, base:number = 2) :number{
    return numero * base
}

// const resultado = multiplicar(5, 2);


// console.log(resultado)

interface PersonajeLOR{
    nombre: string;
    hp: number;
    mostrarHp:()=> void;

}

const nuevoPersonaje: PersonajeLOR = {
    nombre: 'Legolas',
    hp: 50,
    mostrarHp(){
        console.log('Puntos de vida:',this.hp);
    }
}

function curar( personaje:PersonajeLOR, curarX:number): void{
    personaje.hp += curarX;
    console.log(personaje);
}

curar( nuevoPersonaje, 10)

nuevoPersonaje.mostrarHp();