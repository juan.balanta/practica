/*
    ===== Código de TypeScript =====
    EN LAS CLASES SE PUEDEN DEFINIR FUNCIONES
    EN LAS INTERFACES NO.

*/


class PersonaNormal{
    constructor (public nombre:string, public direccion:string){
    }
}



class Heroe extends PersonaNormal {
    /*
    ===== Código de TypeScript =====
    // public alterEgo:string;
    // public edad:number;
    // public nombreReal: number;

    es equivalente a decir luego: heroe.alterEgo = "__"

    equivalente a 
*/
    
    constructor (
        public alterEgo:string,
        public edad:number,
        public nombreReal: string
        ){
            super(nombreReal, 'New York, USA');
    }
}



const ironman = new Heroe('Ironman', 36, 'Tony Stark');


console.log(ironman)