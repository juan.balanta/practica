/*
    ===== Código de TypeScript =====
    <T> significa que es de tipo general
    por lo que una función 
    funcion nombre<T>(variable){}
    la variable puede tener cualquier valor
    si se quiere especificar entonces 

    nombre<number>(variable), variable va a ser si o si un numero.

*/


function queTipoSoy<T>(argumento: T){
    return argumento;
}

let soyString = queTipoSoy('Hola Mundo');
let soyNumero = queTipoSoy(100);
let soyArreglo = queTipoSoy([1,2,3,4,5,6,7,8,9,0]);

let soyExplicito = queTipoSoy<number>(4);
