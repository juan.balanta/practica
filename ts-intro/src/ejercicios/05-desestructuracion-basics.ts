/*
    ===== Código de TypeScript =====
*/

interface Reproductor{
    volumen: number;
    segundo: number;
    cancion: string;
    detalles: Detalles;
}
interface Detalles{
    autor:string;
    anio: number;
}

const reproductor: Reproductor={
    volumen: 90,
    segundo: 36,
    cancion: 'Mess',
    detalles: {
        autor: 'Ed Sheeran',
        anio: 2015
    }
}

const{volumen,segundo,cancion, detalles}=reproductor;

/*
TAMBIEN SIRVE

const{volumen,segundo,cancion, detalles:{autor}}=reproductor;
console.log('El autor es :',autor)

const{volumen,segundo,cancion, detalles:{autor:autorDetalles}}=reproductor;
console.log('El autor es :',autorDetalles)

*/




const{autor, anio}=detalles;


const dbz:string[] = ['Goku', 'Vegeta', 'Trunks']

const [, , pj]= dbz

console.log('El volumen actual es :', volumen)
console.log('El segundo actual es :', segundo)
console.log('La cancion actual es :', cancion)
console.log('El autor es :',pj)
console.log('El anio de la cancion es :', anio)
