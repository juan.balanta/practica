import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'

})
export class AppComponent {
  public titulo:string = 'Contador App';
  contador:number = 10;
  base:number = 5;
  acumular(valor:number){
    this.contador += valor;
  }
}

