import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html'
})
export class ListadoComponent{
  // backUp: string[] = [];
  heroeBorrado:string = '';
  heroes: string[] = ['Ironman', 'Spiderman', 'Hulk', 'Thor', 'Wolverine'];
  borrarHeroe(){
    this.heroeBorrado=this.heroes.shift() || '';
  };
  // borrarHeroe(){
  //  //  mi solución a la tarea, está deja una copia de lo que borra
  //  //  y te deja 
  //   console.log('Borrando....');
  //   const [uno, dos, tres, cuatro, cinco] = this.heroes
  //   // this.heroes.splice(0,1);
  //   this.temporal = this.heroes.pop()
  //   this.backUp.push( this.temporal|| ' ');
  //   console.log(this.backUp);

  // };

  // resetHeroe(){
  //   this.backUp.reverse();
  //   this.backUp.forEach(element => { this.heroes.push(element); 
      
  //   });
  //   console.log(this.backUp);
  //   this.backUp=[];
  //   this.temporal='';
  //   };
    
}
